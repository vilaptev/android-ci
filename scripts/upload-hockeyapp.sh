#!/usr/bin/env bash

GRADLE="./gradlew"
cd ..
$GRADLE clean
$GRADLE assemble
cd app/build/outputs/apk/debug
curl \
  -F "status=2" \
  -F "notify=1" \
  -F "notes=Some new features and fixed bugs." \
  -F "notes_type=0" \
  -F "ipa=@app-debug.apk" \
  -H "X-HockeyAppToken: 8d01fb69da3e41e98a950a594e18dede" \
  https://rink.hockeyapp.net/api/2/apps/eb4db1909877481da5dfaefd4cb2947d/app_versions/upload
